﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CheckoutKata
{
    public class Checkout : ICheckout
    {
        private Dictionary<string, BasketItem> itemDictionary;
        private List<BasketItem> basketItems = new List<BasketItem>();

        public Checkout(Dictionary<string, BasketItem> itemDictionary)
        {
            this.itemDictionary = itemDictionary;
        }

        public int GetTotalPrice()
        {
            int price = 0;

            foreach (var basketItem in basketItems.Distinct())
            {
                int itemCount = basketItems.Count(bi => bi.Sku == basketItem.Sku);

                price += basketItem.Offer?.CalculatePrice(itemCount, basketItem.Price) ?? basketItem.Price * itemCount;
            }

            return price;
        }

        public void Scan(string itemSku)
        {
            if (itemDictionary.ContainsKey(itemSku))
            {
                basketItems.Add(itemDictionary[itemSku]);
            }
        }
    }
}
