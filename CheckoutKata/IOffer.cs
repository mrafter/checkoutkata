﻿namespace CheckoutKata
{
    public interface IOffer
    {
        int CalculatePrice(int itemQuantity, int itemPrice);
    }
}
