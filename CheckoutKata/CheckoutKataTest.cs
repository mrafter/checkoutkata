using System;
using System.Collections.Generic;
using Xunit;

namespace CheckoutKata
{
    public class CheckoutKataTest
    {
        private readonly Checkout checkout = new Checkout(TestData.ItemDictionary);

        [Fact]
        public void GetTotalPrice_EmptyCart_Returns0()
        {
            // Arrange

            // Act
            var result = checkout.GetTotalPrice();

            // Assert
            Assert.Equal(0, result);
        }

        [Fact]
        public void GetTotalPrice_ScanNonExistentItem_Returns0()
        {
            // Arrange
            checkout.Scan("asfasfasf");

            // Act
            var result = checkout.GetTotalPrice();

            // Assert
            Assert.Equal(0, result);
        }

        [Fact]
        public void GetTotalPrice_ScanOneItem_ReturnsItemPrice()
        {
            // Arrange
            checkout.Scan(TestData.ItemA.Sku);

            // Act
            var result = checkout.GetTotalPrice();

            // Assert
            Assert.Equal(TestData.ItemA.Price, result);
        }

        [Fact]
        public void GetTotalPrice_ScanItemTwice_ReturnsItemPriceTimesTwo()
        {
            // Arrange
            int expectedPrice = TestData.ItemA.Price * 2;
            checkout.Scan(TestData.ItemA.Sku);
            checkout.Scan(TestData.ItemA.Sku);

            // Act
            var result = checkout.GetTotalPrice();

            // Assert
            Assert.Equal(expectedPrice, result);
        }

        [Fact]
        public void GetTotalPrice_ScanMultipleItems_ReturnsSumOfItemPrices()
        {
            // Arrange
            int expectedPrice = TestData.ItemA.Price + TestData.ItemB.Price;
            checkout.Scan(TestData.ItemA.Sku);
            checkout.Scan(TestData.ItemB.Sku);

            // Act
            var result = checkout.GetTotalPrice();

            // Assert
            Assert.Equal(expectedPrice, result);
        }

        [Fact]
        public void GetTotalPrice_ScanMultipleItemsWithOffers_ReturnsReducedPrice()
        {
            // Arrange
            int expectedPrice = 130 + 45;

            var basketItems = new List<BasketItem>
            {
                TestData.ItemA, TestData.ItemA, TestData.ItemA, // 130
                TestData.ItemB, TestData.ItemB, // 45
            };

            basketItems.ForEach(item => checkout.Scan(item.Sku));

            // Act
            var result = checkout.GetTotalPrice();

            // Assert
            Assert.Equal(expectedPrice, result);
        }

        [Fact]
        public void GetTotalPrice_ScanMultipleItemsWithOffersAndIncompleteOffers_OnlyReducesCompleteOffers()
        {
            // Arrange
            var basketItems = new List<BasketItem>
            {
                TestData.ItemA, TestData.ItemA, TestData.ItemA, TestData.ItemA, // 180
                TestData.ItemB, TestData.ItemB, TestData.ItemB, // 75
            };

            basketItems.ForEach(item => checkout.Scan(item.Sku));

            const int expectedPrice = 255; // 180+75

            // Act
            var result = checkout.GetTotalPrice();

            // Assert
            Assert.Equal(expectedPrice, result);
        }

        [Fact]
        public void GetTotalPrice_ScanMultipleItemsWithOffersAndWithoutOffers_ReturnsCorrectPrice()
        {
            // Arrange
            var basketItems = new List<BasketItem>
            {
                TestData.ItemA, TestData.ItemA, TestData.ItemA, TestData.ItemA, // 180
                TestData.ItemB, TestData.ItemB, TestData.ItemB, // 75
                TestData.ItemC, // 20
                TestData.ItemD, TestData.ItemD, TestData.ItemD // 45
            };

            basketItems.ForEach(item => checkout.Scan(item.Sku));

            const int expectedPrice = 320; // 180+75+20+45

            // Act
            var result = checkout.GetTotalPrice();

            // Assert
            Assert.Equal(expectedPrice, result);
        }

        private class TestData
        {
            internal static Dictionary<string, BasketItem> ItemDictionary = new Dictionary<string, BasketItem>
            {
                { ItemA.Sku, ItemA },
                { ItemB.Sku, ItemB },
                { ItemC.Sku, ItemC },
                { ItemD.Sku, ItemD }
            };

            internal static BasketItem ItemA => new BasketItem("A", 50, new QuantityForPriceOffer(3, 130));

            internal static BasketItem ItemB => new BasketItem("B", 30, new QuantityForPriceOffer(2, 45));

            internal static BasketItem ItemC => new BasketItem("C", 20);
            internal static BasketItem ItemD => new BasketItem("D", 15);
        }
    }
}
