﻿using Xunit;

namespace CheckoutKata
{
    public class QuantityForPriceOfferTest
    {
        private readonly QuantityForPriceOffer offer = new QuantityForPriceOffer(3, 130);

        [Fact]
        void CalculatePrice_OfferIncomplete_ReturnsSummedUnitPrice()
        {
            // Arrange
            const int itemPrice = 50;
            const int itemQuantity = 2;
            const int expectedPrice = 100; // unit price * 2

            // Act
            var result = offer.CalculatePrice(itemQuantity, itemPrice);

            // Assert
            Assert.Equal(expectedPrice, result);
        }

        [Fact]
        void CalculatePrice_OfferComplete_ReturnsReducedPrice()
        {
            // Arrange
            const int itemPrice = 50;
            const int itemQuantity = 3;
            const int expectedPrice = 130; // reduction price

            // Act
            var result = offer.CalculatePrice(itemQuantity, itemPrice);

            // Assert
            Assert.Equal(expectedPrice, result);
        }

        [InlineData(1, 50)] // unit price
        [InlineData(2, 100)] // unit price * 2
        [InlineData(3, 130)] // offer price        
        [InlineData(4, 180)] // offer price + (unit price)
        [InlineData(5, 230)] // offer price + (unit price * 2)
        [InlineData(6, 260)] // (offer price * 2)
        [InlineData(7, 310)] // (offer price * 2) + unit price
        [InlineData(8, 360)] // (offer price * 2) + (unit price * 2)
        [Theory]
        void CalculatePrice_OfferCompletePlusIncompleteOffers_OnlyReducesCompleteOffers(int itemQuantity, int expectedPrice)
        {
            // Arrange
            const int itemPrice = 50;

            // Act
            var result = offer.CalculatePrice(itemQuantity, itemPrice);

            // Assert
            Assert.Equal(expectedPrice, result);
        }
    }
}
